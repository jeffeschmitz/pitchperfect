//
//  RecordedAudio.swift
//  PitchPerfect
//
//  Created by Jeff Schmitz on 6/27/15.
//
//

import Foundation

class RecordedAudio : NSObject {
    var filePathUrl: NSURL!
    var title: String!
    
    // Initializer
    init(filePathUrl: NSURL, title: String) {
        self.filePathUrl = filePathUrl
        self.title = title
    }
}