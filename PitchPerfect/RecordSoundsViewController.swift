//
//  RecordSoundsViewController.swift
//  PitchPerfect
//
//  Created by Jeff Schmitz on 5/20/15.
//
//

import UIKit
import AVFoundation

class RecordSoundsViewController: UIViewController, AVAudioRecorderDelegate {
    @IBOutlet weak var recordingLabel: UILabel!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var tapMicrophoneLabel: UILabel!
    
    var audioRecorder:AVAudioRecorder!
    var recordedAudio:RecordedAudio!
    
    override func viewWillAppear(animated: Bool) {
        recordingLabel.hidden = true
        stopButton.hidden = true
        tapMicrophoneLabel.hidden = false
        recordButton.enabled = true        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "stopRecording" {
            let playSoundsViewController = segue.destinationViewController as! PlaySoundsViewController
            let data = sender as! RecordedAudio
            playSoundsViewController.receivedAudio = data
        }
    }
    
    @IBAction func recordAudio(sender: UIButton) {
        // Hide labels when user tap's on microphone
        tapMicrophoneLabel.hidden = true
        stopButton.hidden = false
        recordingLabel.hidden = false
        recordButton.enabled = false
        
        // The full relative App Path on the device
        let directoryPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        
        // Name of the audio file the user will make when they click record
        var recordingName = "my_audio.wav"
        var pathArray = [directoryPath, recordingName]
        
        // The fully relative file path to the audio file on the device
        let filePath = NSURL.fileURLWithPathComponents(pathArray)
        
        // Print file path out for debugging
        println(filePath)
        
        // Setup audio session
        var session = AVAudioSession.sharedInstance()
        session.setCategory(AVAudioSessionCategoryPlayAndRecord, error: nil)
        
        // suggested fix for low volume on iPhone by Alex Paul - https://github.com/alexpaul/PitchPerfect
        session.overrideOutputAudioPort(AVAudioSessionPortOverride.Speaker, error: nil)
        
        // Initialize and prepare the recorder
        audioRecorder = AVAudioRecorder(URL: filePath, settings: nil, error: nil)
        audioRecorder.delegate = self
        audioRecorder.meteringEnabled = true
        audioRecorder.prepareToRecord()
        audioRecorder.record()
    }
    
    @IBAction func stopAudio(sender: UIButton) {
        recordingLabel.hidden = true
        audioRecorder.stop()
        var audioSession = AVAudioSession.sharedInstance()
        audioSession.setActive(false, error: nil)
    }
    
    // MARK: AVAudioRecorderDelegate
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder!, successfully flag: Bool) {
        if(flag) {
            // save audio
            recordedAudio = RecordedAudio(filePathUrl: recorder.url, title: recorder.url.lastPathComponent!)
            
            // move to next scene throug a segue
            self.performSegueWithIdentifier("stopRecording", sender: recordedAudio)
        } else {
            println("Recording was not successful")
            recordButton.enabled = true
            stopButton.hidden = true
        }
    }
}